const config = require('./data');

const templates = [];
let counter = 0;

const getTemplate = (diagCase) => {
    diagCase.cnt = counter++;

    const {cnt, title, body, actions} = diagCase;

    if (actions && actions.length) {
        actions.forEach((action) => getTemplate(action))
    }

    templates.push(`
const case${cnt} = new DiagnosticCase({
    title: '${title}',
    body: '${body}',
    ${actions && actions.length ? `actions: [${actions.map((action) => `case${action.cnt}`).join(', ')}],` : ''}
});
`);
};

getTemplate(config);
const fs = require('fs');
const content = fs.readFileSync('./index.html').toString();
fs.writeFileSync('./index.html', content.replace(/\/\*\*__CONFIG_PLACEHOLDER__\*\//, templates.join('')));
